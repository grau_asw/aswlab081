package springFront;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class JSONController {

	
	@RequestMapping(value="/jsonexample", method=RequestMethod.GET, produces="application/json")
	@ResponseBody public Object homePageJson() {
		Map<String,String> result = new HashMap<String,String>();
		result.put("status", "success");
		result.put("content", "It Works!");
		return result;
	}

	
}
